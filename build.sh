#!/bin/bash -ex

# Figure out what the lastest dmg is
category="Video"
description="Compact pocket sized, external, low cost VGA frame grabber capable of capturing output from virtually any VGA source—such as a Windows PC, a Mac OSX, a Unix machine, an em­bed­ded system, a medical device, scientific or lab equipment, and more."
url=`./finder.sh`

# download it
curl -o epiphan.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' ${url}

plist=`pwd`/epiphan.plist
/usr/local/munki/makepkginfo epiphan.dmg > "${plist}"

# Obtain version info
version=`defaults read "${plist}" version`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/EpiphanDrivers-${version}.dmg"
defaults write "${plist}" name "EpiphanDrivers"
defaults write "${plist}" display_name "Epiphan VGA2USB drivers"
defaults write "${plist}" minimum_os_version "10.7.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" unattended_install -bool YES
defaults write "${plist}" description -string "${description}"
defaults write "${plist}" category -string "${category}"

# Add Blocking Applications array
BlockApp1="Cameraman.app"
defaults write "${plist}" blocking_applications -array "${BlockApp1}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv epiphan.dmg EpiphanDrivers-${version}.dmg
mv "${plist}" EpiphanDrivers-${version}.plist
