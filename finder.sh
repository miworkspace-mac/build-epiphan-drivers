#!/bin/bash

NEWLOC=`curl -s http://www.epiphan.com/products/frame-grabbers/vga2usb-hr/download/ | grep -o "Epiphan.*\.dmg" -m 3 | tail -1`

if [ "x${NEWLOC}" != "x" ]; then
	echo "http://www.epiphan.com/downloads/beta/${NEWLOC}"
fi