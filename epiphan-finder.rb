#!/usr/bin/env ruby

require "rubygems"
require "nokogiri"
require "open-uri"

download_page = "http://www.epiphan.com/products/frame-grabbers/vga2usb-hr/download/"

downloads = {}

# Load the page, parse with Nokogiri
html = Nokogiri::HTML(open(download_page))

# Find every DMG on the page, stash it by version string
html.xpath('//a').each do |link|
  if link['href'].match(/EpiphanMacOSX.*\.dmg$/)
      version = /EpiphanMacOSX-(.*)\.dmg/.match(link['href'])[1]
      version = Gem::Version.new(version)
      downloads[version] = link['href']
  end
end

highest_version = downloads.keys.max

puts URI.join(download_page, downloads[highest_version])
